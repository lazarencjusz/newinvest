var express = require('express');
var app = express();

app.get('/*', function(req, res,next) {
    res.sendfile('.' + req.url);
});

app.listen(9000, function() {
    console.log('Example app listening on port 9000!');
});
