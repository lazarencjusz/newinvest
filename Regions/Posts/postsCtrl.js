app.controller("postsCtrl", function($scope, $rootScope, $routeParams, $sce, $http) {
    $rootScope.searching = true;
    $scope.notFound = false;
    $scope.username = $routeParams.username;
    $http({
        method: "GET",
        url: "http://" + $scope.username + ".tumblr.com/api/read/json#_=_"
    }).then((response) => {
            let data = response.data;
            let dataReplaced = data.replace(new RegExp('-', 'g'), '_');
            eval(dataReplaced);
            tumblr_api_read.posts.forEach(post => {
                post.unix_timestamp = new Date(post.unix_timestamp * 1000);
                switch (post.type) {
                    case 'regular':
                        {
                            post.regular_body = $sce.trustAsHtml(post.regular_body)
                            break;
                        }
                    case 'link':
                        {
                            post.link_description = $sce.trustAsHtml(post.regular_body)
                            break;
                        }
                    case 'audio':
                        {
                            post.audio_caption = $sce.trustAsHtml(post.audio_caption)
                            post.audio_embed = $sce.trustAsHtml(post.audio_embed)
                            break;
                        }
                    case 'video':
                        {
                            post.video_player_250 = $sce.trustAsHtml(post.video_player_250)
                            post.video_caption = $sce.trustAsHtml(post.video_caption)
                            break;
                        }
                }
            })
            $scope.data = tumblr_api_read.posts
            tumblr_api_read = undefined;
            $rootScope.searching = false;
            // console.log('$scope.data', $scope.data);
        },
        () => {
            $scope.notFound = true;
            $rootScope.searching = false;
        })
});
