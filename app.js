var app = angular.module("app", ["ngRoute"]);
app.config(
    function($routeProvider) {
        $routeProvider.
        when('/search/:username', {
            templateUrl: './Regions/Posts/posts.html',
            controller: 'postsCtrl'
        }).
        otherwise({
            redirectTo: '/'
        });
    }

);
